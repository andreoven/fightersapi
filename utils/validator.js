const constraints = {
    name: {
        presence: {
            allowEmpty: false
        },
        length: {
            minimum: 1
        }
    },
    health: {
        presence: {
            allowEmpty: false
        },
        numericality: true
    },
    attack: {
        presence: {
            allowEmpty: false
        },
        numericality: true
    },
    defense: {
        presence: {
            allowEmpty: false
        },
        numericality: true
    },
    source: {
        presence: {
            allowEmpty: false
        },
        url: true,
        length: {
            minimum: 1
        }
    }
};

const idConstraints = {
    id: {
        presence: {
            allowEmpty: false
        },
        numericality: true
    }
};

module.exports = {
    constraints
};