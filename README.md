# FighersAPI

This is a Node.js api server. It was made as a task for Binary Studio Academy.

# How to use
- GET: /user - Get all the users(fighters).
- GET: /user/:id - Get one user by id.
- POST: /user - Create a new user(Sent user data without id, id generates automatically).
- PUT: /user/:id - Update the existing user by id.
- DELETE: /user/:id - Delete the user by id.

## Instalation

Simply clone the repository and do
```
$ npm install
```

## Built with:
- Node.js
- Express
- Validate.js
- Lodash
- Cors
