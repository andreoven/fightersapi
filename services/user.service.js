const validate = require("validate.js");
var _ = require('lodash');

const { getAllUsers } = require("../repositories/user.repository");
const { rewriteUsers } = require("../repositories/user.repository");
const { constraints } = require("../utils/validator");
const { idConstraints } = require("../utils/validator");


const getUsers =  () => {
    return getAllUsers();
};

const getUserById = (id) => {
    if (validate(id, idConstraints) === undefined){
        let data = getAllUsers();
        const user = data.filter(user => user._id === id);
        if (user.length <= 0) {
            return false;
        }
        return user;
    } else {
        return false;
    }
};

const createUser = (userData) => {
    const allUsers = getAllUsers();
    let newUser;
    if (_.isArray(userData)) {
        newUser = userData[0];
    } else {
        newUser = userData;
    }
    if (validate(newUser, constraints) === undefined && _.size(newUser) === 5){
        newUser._id = allUsers.length + 1;
        allUsers.push(newUser);
        rewriteUsers(JSON.stringify(allUsers));
        return true;
    } else {
        return false;
    }
};

const updateUser = (id, userData) => {
    const allUsers = getAllUsers();
    let newUser;
    if (_.isArray(userData)) {
        newUser = userData[0];
    } else {
        newUser = userData;
    }
    if (validate(newUser, constraints) === undefined && Object.keys(newUser).length === 5){
        let userIndex = _.findIndex(allUsers, element => element._id == id);
        allUsers[userIndex].name = newUser.name;
        allUsers[userIndex].health = newUser.health;
        allUsers[userIndex].attack = newUser.attack;
        allUsers[userIndex].defense = newUser.defense;
        allUsers[userIndex].source = newUser.source;
        rewriteUsers(JSON.stringify(allUsers));
        return true;
    } else {
        return false;
    }
};

const deleteUser = (id) => {
    const allUsers = getAllUsers();
    let userIndex = _.findIndex(allUsers, element => element._id == id);
    allUsers.splice(userIndex, 1);
    rewriteUsers(JSON.stringify(allUsers));
    return true;
};


module.exports = {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
};